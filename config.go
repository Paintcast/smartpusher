package main

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
)

type Config struct {
	Redis    RedisConfig    `yaml:"redis"`
	Mysql    MySqlConfig    `yaml:"mysql"`
	RabbitMQ RabbitMQConfig `yaml:"rabbitmq"`
	Pusher   PusherConfig   `yaml:"pusher"`
}

type MySqlConfig struct {
	Host  string `yaml:"host"`
	Port  int    `yaml:"port"`
	DB    string `yaml:"db"`
	Login string `yaml:"login"`
	Pass  string `yaml:"pass"`
}

type RedisConfig struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
	DB   int    `yaml:"db"`
	TTL  int    `yaml:"ttl"`
}

type RabbitMQConfig struct {
	Host         string `yaml:"host"`
	Port         int    `yaml:"port"`
	VHost        string `yaml:"vhost"`
	Login        string `yaml:"login"`
	Pass         string `yaml:"pass"`
	QueueName    string `yaml:"queue"`
	PrefetchOn   int    `yaml:"prefetch_on"`
	PrefetchSize int    `yaml:"prefetch_size"`
}

type PusherConfig struct {
	Cpu        float64 `yaml:"cpu"`
	PrivateKey string  `yaml:"privatekey"`
	Subscriber string  `yaml:"subscriber"`
	Workers    int     `yaml:"workers"`
}

func ReadConfig() *Config {
	var buf []byte
	var err error

	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)

	buf, err = ioutil.ReadFile(fmt.Sprint(exPath, "/config_prod.yml"))
	if err != nil {
		buf, err = ioutil.ReadFile(fmt.Sprint(exPath, "/config_dev.yml"))
		if err != nil {
			log.Println("no config file was loaded")
		}
	}

	var config Config
	if len(buf) > 0 {
		// Unmarshal yml
		err = yaml.Unmarshal(buf, &config)
		if err != nil {
			log.Println("cannot unmarshal YML config: ", err)
		}
	}

	if value, ok := os.LookupEnv("REDIS_HOST"); ok {
		config.Redis.Host = value
	}
	if value, ok := os.LookupEnv("REDIS_PORT"); ok {
		intVal, err := strconv.Atoi(value)
		if err != nil {
			log.Println("cannot read REDIS_PORT")
		} else {
			config.Redis.Port = intVal
		}
	}
	if value, ok := os.LookupEnv("REDIS_DB"); ok {
		intVal, err := strconv.Atoi(value)
		if err != nil {
			log.Println("cannot read REDIS_DB")
		} else {
			config.Redis.DB = intVal
		}
	}
	if value, ok := os.LookupEnv("REDIS_TTL"); ok {
		intVal, err := strconv.Atoi(value)
		if err != nil {
			log.Println("cannot read REDIS_TTL")
		} else {
			config.Redis.TTL = intVal
		}
	}

	if value, ok := os.LookupEnv("MYSQL_HOST"); ok {
		config.Mysql.Host = value
	}
	if value, ok := os.LookupEnv("MYSQL_PORT"); ok {
		intVal, err := strconv.Atoi(value)
		if err != nil {
			log.Println("cannot read MYSQL_PORT")
		} else {
			config.Mysql.Port = intVal
		}
	}
	if value, ok := os.LookupEnv("MYSQL_DB"); ok {
		config.Mysql.DB = value
	}
	if value, ok := os.LookupEnv("MYSQL_LOGIN"); ok {
		config.Mysql.Login = value
	}
	if value, ok := os.LookupEnv("MYSQL_PASS"); ok {
		config.Mysql.Pass = value
	}

	if value, ok := os.LookupEnv("RABBIMQ_HOST"); ok {
		config.RabbitMQ.Host = value
	}
	if value, ok := os.LookupEnv("RABBIMQ_PORT"); ok {
		intVal, err := strconv.Atoi(value)
		if err != nil {
			log.Println("cannot read RABBIMQ_PORT")
		} else {
			config.RabbitMQ.Port = intVal
		}
	}
	if value, ok := os.LookupEnv("RABBIMQ_VHOST"); ok {
		config.RabbitMQ.VHost = value
	}
	if value, ok := os.LookupEnv("RABBIMQ_LOGIN"); ok {
		config.RabbitMQ.Login = value
	}
	if value, ok := os.LookupEnv("RABBIMQ_PASS"); ok {
		config.RabbitMQ.Pass = value
	}
	if value, ok := os.LookupEnv("RABBIMQ_QUEUE_NAME"); ok {
		config.RabbitMQ.QueueName = value
	}
	if value, ok := os.LookupEnv("RABBIMQ_PREFETCH_ON"); ok {
		intVal, err := strconv.Atoi(value)
		if err != nil {
			log.Println("cannot read RABBIMQ_PREFETCH_ON")
		} else {
			config.RabbitMQ.PrefetchOn = intVal
		}
	}
	if value, ok := os.LookupEnv("RABBIMQ_PREFETCH_SIZE"); ok {
		intVal, err := strconv.Atoi(value)
		if err != nil {
			log.Println("cannot read RABBIMQ_PREFETCH_SIZE")
		} else {
			config.RabbitMQ.PrefetchSize = intVal
		}
	}

	if value, ok := os.LookupEnv("PUSHER_CPU"); ok {
		floatVal, err := strconv.ParseFloat(value, 64)
		if err != nil {
			log.Println("cannot read PUSHER_CPU")
		} else {
			config.Pusher.Cpu = floatVal
		}
	}

	if value, ok := os.LookupEnv("PUSHER_PKEY"); ok {
		config.Pusher.PrivateKey = value
	}
	if value, ok := os.LookupEnv("PUSHER_SUBSCR"); ok {
		config.Pusher.Subscriber = value
	}
	if value, ok := os.LookupEnv("PUSHER_WORKERS"); ok {
		intVal, err := strconv.Atoi(value)
		if err != nil {
			log.Println("cannot read PUSHER_WORKERS")
		} else {
			config.Pusher.Workers = intVal
		}
	}

	return &config
}
