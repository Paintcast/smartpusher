package main

import (
	"fmt"
	"github.com/streadway/amqp"
	"log"
)

func GetRabbitMQConn(conf *Config) *amqp.Connection {
	url := fmt.Sprint("amqp://", conf.RabbitMQ.Login, ":", conf.RabbitMQ.Pass, "@", conf.RabbitMQ.Host, ":", conf.RabbitMQ.Port, "/", conf.RabbitMQ.VHost)
	conn, err := amqp.Dial(url)
	if err != nil {
		log.Fatalln("Failed to connect to RabbitMQ", err)
	}
	log.Println("RabbitMQ connection created")
	return conn
}

func GetRabbitMQChan(rc *amqp.Connection) *amqp.Channel {
	channel, err := rc.Channel()
	if err != nil {
		log.Fatalln("Failed to create RabbitMQ channel", err)
	}
	log.Println("RabbitMQ channel created")
	return channel
}

func GetRabbitMQQueueAndConsume(ch *amqp.Channel, queue string) <-chan amqp.Delivery {
	q, err := ch.QueueDeclare(
		queue, // name
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		log.Fatalln("Failed to declare a queue", err)
	}

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)

	if err != nil {
		log.Fatalln("Failed to register a consumer", err)
	}

	return msgs
}
