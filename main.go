package main

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"log"
	"runtime"
	"smartpusher/webpush-go"
	"sync"
	"time"
)

type Message struct {
	ID       string `json:"id"`
	DomainID int    `json:"did"`
	Endpoint string `json:"endpoint"`
	P256dh   string `json:"P256dh"`
	Auth     string `json:"auth"`
	Hash     string `json:"hash"`
}

type RedisMsg struct {
	ID       string `json:"id"`
	DomainID int    `json:"did"`
}

func main() {
	conf := ReadConfig()
	db := GetDB(conf)
	defer db.Close()

	cpu := runtime.NumCPU()
	usedCpu := int(float64(cpu) * conf.Pusher.Cpu)
	runtime.GOMAXPROCS(usedCpu)

	log.Println("cpu:", cpu)
	log.Println("used cpu: ", usedCpu)
	log.Println("SmartPusher rollin'..")

	rabbitConn := GetRabbitMQConn(conf)
	defer rabbitConn.Close()

	rabbitChan := GetRabbitMQChan(rabbitConn)
	defer rabbitChan.Close()

	if conf.RabbitMQ.PrefetchOn == 1 {
		rabbitChan.Qos(
			conf.RabbitMQ.PrefetchSize,
			0,
			true,
		)
	}

	pushOptions := getPushOptions(conf)

	log.Println("Stable no rabbit reconnect. Amount of workers: ", conf.Pusher.Workers)

	// run workers
	var wg sync.WaitGroup
	wg.Add(conf.Pusher.Workers)
	for i := 0; i < conf.Pusher.Workers; i++ {
		log.Println("worker started #", i)
		msgChan := GetRabbitMQQueueAndConsume(rabbitChan, conf.RabbitMQ.QueueName)
		go worker(&wg, msgChan, pushOptions, conf.Redis.TTL, db)
	}

	log.Println("Stable no rabbit reconnect. waiting for workers to stop.")

	wg.Wait()
	log.Println("that's all folks")
}

func worker(wg *sync.WaitGroup, msgQueue <-chan amqp.Delivery, pushOptions *webpush.Options, ttl int, db *DB) {
	defer wg.Done()
	for {
		amqpMsg, ok := <-msgQueue
		if !ok {
			log.Println("worker: message channel closed")
			return
		}
		var msg Message
		if err := json.Unmarshal(amqpMsg.Body, &msg); err != nil {
			log.Println("cannot decode json:", err)
			err = amqpMsg.Ack(false)
			if err != nil {
				log.Println("1 ack=false failed, going to next message")
			}
			continue
		}
		if !db.PingMySQL() {
			err := amqpMsg.Ack(true)
			if err != nil {
				log.Println("1 ack=true failed")
			}
			time.Sleep(time.Second * 2)
			continue
		}
		code, err := sendPush(getSubscription(msg.Endpoint, msg.P256dh, msg.Auth), pushOptions)
		if err != nil {
			log.Println("push failed: ", err)
			err = amqpMsg.Ack(true)
			if err != nil {
				log.Println("2 ack=true failed, going to next message")
			}
			continue
		}
		checkStatusCode(code, &msg, ttl, db)
		err = amqpMsg.Ack(false)
		if err != nil {
			log.Println("2 ack=false failed")
		}
	}
}

func getPushOptions(conf *Config) *webpush.Options {
	return &webpush.Options{
		Subscriber:      conf.Pusher.Subscriber,
		VAPIDPrivateKey: conf.Pusher.PrivateKey,
	}
}

func getSubscription(endpoint, p256dh, auth string) *webpush.Subscription {
	return &webpush.Subscription{
		Endpoint: endpoint,
		Keys: webpush.Keys{
			P256dh: p256dh,
			Auth:   auth,
		},
	}
}

func sendPush(subscription *webpush.Subscription, options *webpush.Options) (int, error) {
	response, err := webpush.SendNotification([]byte(""), subscription, options)
	if err != nil {
		return 0, err
	}
	return response.StatusCode, nil
}

func checkStatusCode(code int, msg *Message, ttl int, db *DB) {
	if code == 404 || code == 410 {
		db.writeFailToMySQL(msg.ID)
		db.writeToMySQL(msg.DomainID, 0, 1)
		return
	}
	if code >= 200 && code <= 299 {
		db.writeToMySQL(msg.DomainID, 1, 0)
		if db.PingRedis() {
			redisMsg := RedisMsg{
				ID:       msg.ID,
				DomainID: msg.DomainID,
			}
			db.writeToRedis(redisMsg, msg.Hash, ttl)
		}
	} else {
		db.writeToMySQL(msg.DomainID, 0, 1)
	}
}
