# SmartPusher

## Requirements
- [Go 1.8+](https://golang.org/)
- [webpush-go](https://github.com/SherClockHolmes/webpush-go)
- [redigo](https://github.com/gomodule/redigo)
- [go-sql-driver](https://github.com/go-sql-driver/mysql)

## Installation

```sh
$ git clone git@gitlab.com:Paintcast/smartpusher.git
$ cd smartpusher
$ go get -d ./...
$ go build -o smartpusher
$ ./smartpusher
```

## Usage
You have to fill configuration file config_dev.yml
For production just make file with the same structure, but name it config_prod.yml