package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gomodule/redigo/redis"
	"log"
	"runtime"
	"strconv"
	"time"
)

type DB struct {
	MySQL *sql.DB
	Redis *redis.Pool
}

func GetDB(c *Config) *DB {
	return &DB{
		MySQL: InitMySQL(c),
		Redis: InitRedis(c),
	}
}

func InitMySQL(c *Config) *sql.DB {
	pauseDuration := 5
	reTriesAmount := 5
	connString := getConnectionUrl(c)
	for {
		db, err := sql.Open("mysql", connString)
		if err != nil {
			log.Println(err)
			log.Println("waiting for", pauseDuration, "seconds before reconnection to SQL server. Amount of retries:", reTriesAmount)
			time.Sleep(time.Duration(pauseDuration) * time.Second)
			pauseDuration += 5
			reTriesAmount--
			if reTriesAmount <= 0 {
				log.Fatal("failed to dial SQL server.")
				break
			}
			continue
		}
		return db
	}
	return nil
}

func getConnectionUrl(c *Config) string {
	if runtime.GOOS == "linux" {
		return fmt.Sprintf("%s:%s@unix(/var/lib/mysql/mysql.sock)/%s?loc=Local", c.Mysql.Login, c.Mysql.Pass, c.Mysql.DB)
	}
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", c.Mysql.Login, c.Mysql.Pass, c.Mysql.Host, c.Mysql.Port, c.Mysql.DB)
}

func InitRedis(c *Config) *redis.Pool {
	connString := fmt.Sprint(c.Redis.Host, ":", strconv.Itoa(c.Redis.Port))
	return &redis.Pool{
		MaxIdle:     10000,
		IdleTimeout: 1 * time.Second,
		Dial: func() (redis.Conn, error) {
			conn, err := redis.Dial("tcp", connString)
			if err != nil {
				log.Fatal("failed to dial noSQL server:", err)
			}
			_, err = conn.Do("SELECT", c.Redis.DB)
			if err != nil {
				conn.Close()
				return nil, err
			}
			return conn, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func (db *DB) Close() {
	var err error
	if err = db.MySQL.Close(); err != nil {
		log.Fatal("cannot close connection to MySQL:", err)
	}

	if err = db.Redis.Close(); err != nil {
		log.Fatal("cannot close connection to Redis:", err)
	}
}

func (db DB) writeToRedis(msg RedisMsg, hash string, ttl int) {
	conn := db.Redis.Get()
	defer conn.Close()
	redisMsgBytes, err := json.Marshal(msg)
	if err != nil {
		log.Println(err)
		return
	}
	key := fmt.Sprint("{uid:", hash, "}:subscriber")
	conn.Do("SETEX", key, ttl, redisMsgBytes)
}

func (db DB) writeToMySQL(domainId, pushSuccess, pushFail int) {
	insert, err := db.MySQL.Prepare("INSERT INTO push_stat (day,domain_id,push_success,push_fail,ad_success,ad_fail) VALUES (?,?,?,?,0,0) ON DUPLICATE KEY UPDATE push_success = push_success + ?, push_fail = push_fail + ?, ad_success = ad_success + 0, ad_fail = ad_fail + 0")
	if err != nil {
		log.Println(err)
		return
	}
	_, err = insert.Exec(time.Now().Format("20060102"), domainId, pushSuccess, pushFail, pushSuccess, pushFail)
	if err != nil {
		log.Println(err)
	}
}

func (db DB) writeFailToMySQL(id string) {
	upd, err := db.MySQL.Prepare("DELETE FROM notify.subscriber WHERE id=?")
	if err != nil {
		log.Println(err)
		return
	}
	_, err = upd.Exec(id)
	if err != nil {
		log.Println(err)
	}
}

func (db DB) PingMySQL() bool {
	sqlErr := db.MySQL.Ping()
	if sqlErr != nil {
		log.Println("no ping from MySQL server:", sqlErr)
		return false
	}
	return true
}

func (db DB) PingRedis() bool {
	conn := db.Redis.Get()
	defer conn.Close()

	_, err := redis.String(conn.Do("PING"))
	if err != nil {
		log.Println("no ping from Redis server:", err)
		return false
	}
	return true
}
